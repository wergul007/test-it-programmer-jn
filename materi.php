<?php 
$authors = array(
    array('id'=>1, 'username'=>'jonsnow', 'fullname'=>'Jon Snow'),
    array('id'=>2, 'username'=>'daeny88', 'fullname'=>'Daenerys Targaryen')
);
$articles = array(
    array(
        'id'=>1, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut neque viverra, malesuada nisi ut, fermentum augue. Maecenas ac urna non nunc imperdiet posuere. Morbi sit amet volutpat libero. Duis tincidunt varius malesuada</p><p>Nunc a porttitor nisi, vitae aliquet ex. Nullam mollis metus neque, scelerisque fermentum purus laoreet vestibulum. Cras aliquet rutrum tellus, ultricies porttitor tellus luctus vel. Donec lacus diam, feugiat molestie nulla a, consectetur posuere velit. Etiam lacinia ex at gravida ultrices. Phasellus ullamcorper dignissim purus, faucibus venenatis tellus congue quis.</p><p><a href="#" title="">Link 1</a></p><p>Mauris molestie fringilla dui sit amet fringilla. Nulla id mi malesuada, pharetra justo eu, rutrum nisi. Donec eget ipsum gravida, volutpat quam non, vulputate sem. Nulla gravida ullamcorper rhoncus</p><p>Donec semper posuere cursus. Donec tristique mi in tortor convallis, vel mollis ipsum iaculis. Aliquam in condimentum enim, sed aliquam ipsum</p><p><a href="#" title="">Link 2</a></p>',
        'status'=>true,
        'created_by'=>1
    ),
    array(
        'id'=>2, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>false,
        'created_by'=>1
    ),
    array(
        'id'=>3, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>1
    ),
    array(
        'id'=>4, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>false,
        'created_by'=>1
    ),
    array(
        'id'=>5, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>1
    ),
    array(
        'id'=>6, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>2
    ),
    array(
        'id'=>7, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>2
    ),
    array(
        'id'=>8, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>2
    ),
    array(
        'id'=>9, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>2
    ),
    array(
        'id'=>10, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>2
    ),
    array(
        'id'=>11, 
        'title'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 
        'content'=>'',
        'status'=>true,
        'created_by'=>666
    )
);