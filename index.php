<?php 
include_once("materi.php");
// 1. Gabungkan dua array yang ada pada file materi.php melalui proses looping sehingga menjadi array seperti berikut
/**
 * Contoh output
Array
(
      [0] => Array
          (
                [id] => 1
                [title] => Lorem ipsum dolor sit amet, consectetur adipiscing elit
                [content] => 
                [status] => 1
                [created_by] => Array
                    (
                        [id] => 1
                        [username] => jonsnow
                        [fullname] => Jon Snow                        
                    )
          )
      [1] => Array
         (
                [id] => 1
                [title] => Lorem ipsum dolor sit amet, consectetur adipiscing elit
                [content] => 
                [status] => 1
                [created_by] => Array
                    (
                        [id] => 1
                        [username] => jonsnow
                        [fullname] => Jon Snow                        
                    )
          )
      dan seterusanya...
 )
*/
// 2. Tampilkan semua artikel berdasarkan parameter username = jonsnow, dan status = true
/**
 * Contoh output
Array
(
      [0] => Array
          (
                [id] => 1
                [title] => Lorem ipsum dolor sit amet, consectetur adipiscing elit
                [content] => 
                [status] => 1
                [created_by] => Array
                    (
                        [id] => 1
                        [username] => jonsnow
                        [fullname] => Jon Snow                        
                    )
          )
      [1] => Array
         (
                [id] => 1
                [title] => Lorem ipsum dolor sit amet, consectetur adipiscing elit
                [content] => 
                [status] => 1
                [created_by] => Array
                    (
                        [id] => 1
                        [username] => jonsnow
                        [fullname] => Jon Snow                        
                    )
          )
      dan seterusanya...
 ) 
 *
 */
// 3. Kumpulkan semua link yang ada pada setiap paragraf pada artikel dengan parameter id = 1 sehingga menjadi sebuah array seperti berikut
/**
 * Contoh output
Array
(
    [0] => Array
        (
            [url] => #
            [text] => Link 1
        )

    [1] => Array
        (
            [url] => #
            [text] => Link 2
        )
)
 */
// 4. Buatlah layout seperti pada gambar berikut
